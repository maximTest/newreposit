import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/18/15
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainPage extends CommonPage {

    @FindBy(css ="a[href='/categories']")
     private WebElement buttonNews;

    @FindBy(xpath = ".//span/b")
     private  WebElement innerTextTag;


    MainPage(WebDriver webDriver){
        super(webDriver);
       // waitForEndOfAllAjaxes();

    }

    MainPage openNews(){
        buttonNews.click();
        return new MainPage(webDriver);
    }

    public void searc(){
        webDriver.findElements(By.cssSelector(""));
        webDriver.findElements(By.xpath(""));

    }
    public String getText(){
          return innerTextTag.getAttribute("innerHTML");
    }

}
