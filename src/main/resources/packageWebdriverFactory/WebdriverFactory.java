package packageWebdriverFactory;

import listener.ListenerHighlightsElementsBeforeActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import properties.PropertyLoader;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/17/15
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class WebdriverFactory {


    private WebDriver webdriver;

    public WebDriver getDriver(String browser, String typeOfRun) {
       if(typeOfRun.equals("local")){
           if(browser.equals("firefox")){
               FirefoxProfile profile = new FirefoxProfile();
               try {
                   //for work with https
                   profile.setAcceptUntrustedCertificates(true);
                   //avoid popup
                   profile.setPreference("browser.selfsupport.url","");
                   profile.addExtension(new File("src\\main\\resources\\webdriver\\firebug-2.0.9-fx.xpi"));
                   //avoid popup about updating firebug
                   profile.setPreference("extensions.firebug.currentVersion", "9.9.9");
                   // FirePath
                   profile.addExtension(
                           new File("src\\main\\resources\\webdriver\\firepath-0.9.7.1-fx.xpi"));
               } catch (IOException e) {
                   e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
               }
               webdriver = new FirefoxDriver(profile);
           }
             else {
               System.setProperty("webdriver.chrome.driver", PropertyLoader.loadProperties("chromeDriver.path"));
               webdriver = new ChromeDriver();
           }

       } else {
       DesiredCapabilities capabilities = new DesiredCapabilities();
       capabilities.setBrowserName(browser);
        try {
            webdriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),capabilities );
        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(webdriver);
        eventFiringWebDriver.register(new ListenerHighlightsElementsBeforeActions("green"));
        return eventFiringWebDriver;
    }
}
