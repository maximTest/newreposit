package Properties;

import java.io.IOException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/17/15
 * Time: 1:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class PropertyLoader {

    static final String PATH_PROPERTIES_FILE = "../TestLearningOne.properties";

    public static String loadProperties(String properties){
        Properties property = new Properties();
        try {
            property.load(PropertyLoader.class.getResourceAsStream(PATH_PROPERTIES_FILE));

        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = "";
        if(properties!= null){
            result = property.getProperty(properties);
        }
        return result;
    }
}
