import properties.PropertyLoader;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import packageWebdriverFactory.WebdriverFactory;
import util.GlobalVariable;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/17/15
 * Time: 1:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestBase {

    String timeout;
    String browser;
    String typeOfRun;

    protected WebDriver webDriver;

    TestBase(){
        browser = PropertyLoader.loadProperties("browser.name");
        typeOfRun = PropertyLoader.loadProperties("executionType");

    }

  @BeforeTest(alwaysRun = true)
  public void initDriver(ITestContext context){
      webDriver = new WebdriverFactory().getDriver(browser,typeOfRun);
      webDriver.manage().timeouts().implicitlyWait(getTimeOut(), TimeUnit.SECONDS);
      System.setProperty("org.uncommons.reportng.escape-output", "true");
      context.setAttribute(context.getCurrentXmlTest().getName(), webDriver);
  }


  @AfterMethod(alwaysRun = true)
  public void getScreenAfterFail(ITestResult result){
       Reporter.setCurrentTestResult(result);
      if(result.isSuccess()){
         Reporter.log("Test: " + result.getTestContext().getCurrentXmlTest().getName()
                  + "_" + result.getName(),Reporter.getCurrentTestResult().isSuccess() );
          return;
      }
      Reporter.log("Test: " + result.getTestContext().getCurrentXmlTest().getName()
              + "_" + result.getName(),result.isSuccess());
      Object testClassInstance = result.getInstance();

      if(testClassInstance instanceof TestBase) {
          WebDriver driver =((TestBase)testClassInstance).getDriver();
      }
      String executionType = PropertyLoader.loadProperties("executionType");

      // RemoteWebDriver does not implement the TakesScreenshot class
      // if the driver does have the Capabilities to take a screenshot
      // then Augmenter will add the TakesScreenshot methods to the instance
      if (executionType.equalsIgnoreCase("remote")) webDriver = new Augmenter().augment(webDriver);

      // Create the filename for the screen shots
      Calendar calendar = Calendar.getInstance();

      // The file includes the the test method and the test class
      String testMethodAndTestClass = result.getMethod().getMethodName() + "(" + result.getTestClass().getName() + ")";
      String filename =  webDriver.getTitle()
              + testMethodAndTestClass + "-"
              + calendar.get(Calendar.YEAR) + "-"
              + calendar.get(Calendar.MONTH) + "-"
              + calendar.get(Calendar.DAY_OF_MONTH) + "-"
              + calendar.get(Calendar.HOUR_OF_DAY) + "-"
              + calendar.get(Calendar.MINUTE) + "-"
              + calendar.get(Calendar.SECOND) + "-"
              + ".png";

      File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
      File outputDir = new File(result.getTestContext().getOutputDirectory());
      File saved;
      if (executionType.equalsIgnoreCase("remote")) {
          saved = new File(outputDir.getParent() + "/html", result.getTestContext().getCurrentXmlTest().getName() + "_" + result.getName()+".png");
      }
      else {
          saved = new File(outputDir.getParent() + "\\html", result.getTestContext().getCurrentXmlTest().getName() + "_" + result.getName()+".png");
      }

      // Take the screen shot and then copy the file to the screen shot folder
      try  {
          FileUtils.copyFile(scrFile, saved);
      }
      catch (IOException e)
      {
          e.printStackTrace();
      }
      System.setProperty("org.uncommons.reportng.escape-output", "false");
      Reporter.log("<a href=\"" + saved.getName() + "\"><p align=\"left\">Error screenshot at " + filename + "</p>");
      Reporter.log("<p><img width=\"1024\" src=\"" + saved.getName()  + "\" alt=\"screenshot at " + filename + "\"/></p></a><br />");
  }

  @AfterTest(alwaysRun = true)
  public void tearDown(ITestContext context){
      if(webDriver !=null){
          webDriver.quit();

      }
      context.removeAttribute(context.getCurrentXmlTest().getName());
  }



    public int getTimeOut() {
        timeout = PropertyLoader.loadProperties("timeout");
        if(!timeout.equals("")){
            GlobalVariable.GlobalTimeOut = Integer.parseInt(timeout);
        }
        return GlobalVariable.GlobalTimeOut;
    }

    public WebDriver getDriver() {
        return webDriver;
    }
}
