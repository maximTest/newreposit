import properties.PropertyLoader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: mmal
 * Date: 2/18/15
 * Time: 4:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestOpenNews extends TestBase {
     MainPage mainPage;
   @BeforeClass
    public void testOpenNews(){
         webDriver.navigate().to(PropertyLoader.loadProperties("site.url"));
         mainPage = new MainPage(webDriver);

    }

 /* @Test
  public void testOpenNewsLink(){
      mainPage.openNews();
     assertTrue(true);
  }

    @Test
    public void testOpenNewSLinkOne(){
        mainPage.openNews();
    }*/

    @Test
    public void checkTextField(){
        assertTrue(mainPage.getText().equals("relativity pricing")," Actual result:"
                + mainPage.getText() + " doesn't equal Expected result:" + "relativity pricing \n");
    }

    @Test
    public void checkAnotherTextField(){
        assertTrue(mainPage.getText().equals(" relativity pricing")," Actual result:"
                + mainPage.getText() + " doesn't equal Expected result:" + "relativity pricing \n");
    }
}
